package com.maximemichel.myhome.app

import android.app.Application
import com.maximemichel.myhome.app.conf.stethoConf
import com.maximemichel.myhome.app.conf.timberConf
import com.maximemichel.myhome.presentation.util.NetworkMonitorCallbacks
import com.maximemichel.myhome.presentation.util.extension.connectivityManager
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyHomeApplication : Application() {

    private val activityCallbacks by lazy {
        listOf(
            NetworkMonitorCallbacks(
                connectivityManager()
            )
        )
    }

    override fun onCreate() {
        super.onCreate()
        timberConf()
        stethoConf(this)

        activityCallbacks.forEach(::registerActivityLifecycleCallbacks)
    }
}
