package com.maximemichel.myhome.app

import android.app.Application
import android.content.Context
import com.maximemichel.myhome.domain.di.AppFlavor
import com.maximemichel.myhome.domain.di.AppVersionName
import com.maximemichel.myhome.domain.di.AppVersionNameMinor
import com.maximemichel.myhome.domain.model.Flavor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @AppFlavor
    fun provideFlavor(): Flavor = Flavor.getFlavorFromCode(BuildConfig.FLAVOR)

    @Provides
    @AppVersionNameMinor
    fun provideAppName(@AppFlavor flavor: Flavor): String {
        return when (flavor) {
            Flavor.PROD -> BuildConfig.VERSION_NAME_MINOR
            else -> BuildConfig.VERSION_NAME
        }
    }

    @Provides
    @AppVersionName
    fun provideAppVersionName() = BuildConfig.VERSION_NAME

    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application
}
