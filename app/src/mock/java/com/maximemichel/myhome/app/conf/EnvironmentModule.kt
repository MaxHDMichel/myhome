package com.maximemichel.myhome.app.conf

import android.content.Context
import com.maximemichel.myhome.data.mock.MockManager
import com.maximemichel.myhome.data.mock.MockServerUrl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object EnvironmentModule {

    @Provides
    @MockServerUrl
    fun provideMockManager(@ApplicationContext context: Context) = MockManager().init(context)
}
