package com.maximemichel.myhome.app.conf

import android.content.Context
import com.facebook.stetho.Stetho

fun stethoConf(context: Context) {
    Stetho.initializeWithDefaults(context)
}
