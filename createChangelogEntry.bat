@echo off

:: Batch file used to create a changelog entry
:: First parameter: Affected version
:: Second parameter: The name of the changelog entry
:: Third parameter: The type of the changelog entry
:: Fourth parameter: The description of the changelog entry

:: Example createChangelogEntry.bat 1.0.0 login_screen added "Add the login screen with credentials validation"

SET example= ^echo.^& echo.See example: ^& echo.createChangelogEntry.bat 1.0.0 login_screen added "Add the login screen with credentials validation"
SET template= description: 1972 type: added

IF "%~1"=="" ECHO You must specify the affected version %example% $ EXIT /B
IF "%2"=="" ECHO You must specify the name of the changelog entry %example% $ EXIT /B
IF "%~3"=="" ECHO You must specify the type of the changelog entry %example% $ EXIT /B

FOR %%G IN ("added" "fixed" "changed" "deprecated" "removed" "security" "performance" "other") DO (
    IF /I "%~3"=="%%~G" GOTO VALID_TYPE
)

:INVALID_TYPE
ECHO The type of the changelog entry must be either added, fixed, changed, deprecated, removed, security, performance or other %example%
EXIT /B

:VALID_TYPE

IF "%~4"=="" (echo You must specify the description of the changelog entry & exit /B)

SET FOLDER_PATH=.\.changelog\%1
SET FILE_PATH=%FOLDER_PATH%\%2.yml

IF NOT EXIST %FOLDER_PATH% MKDIR %FOLDER_PATH%

echo description: %~4 > %FILE_PATH%
echo type: %3 >> %FILE_PATH%