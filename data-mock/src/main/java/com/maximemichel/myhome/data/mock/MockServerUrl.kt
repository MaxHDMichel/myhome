package com.maximemichel.myhome.data.mock

import javax.inject.Qualifier

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class MockServerUrl
