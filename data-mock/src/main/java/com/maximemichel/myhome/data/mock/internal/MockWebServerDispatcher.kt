package com.maximemichel.myhome.data.mock.internal

import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import timber.log.Timber

class MockWebServerDispatcher(
    private val assets: AssetProvider
) : Dispatcher() {

    @Suppress("ComplexMethod", "LongMethod")
    override fun dispatch(request: RecordedRequest): MockResponse {
        val path = request.path.orEmpty()

        return when {
            path == "" -> assets.createEmptyResponse()
            else -> {
                Timber.w(
                    "Mocked URL not handled for path (%s), returning empty response",
                    request.path
                )
                assets.createEmptyResponse()
            }
        }
    }
}
