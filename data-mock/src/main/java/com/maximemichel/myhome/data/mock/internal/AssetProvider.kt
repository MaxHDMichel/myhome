package com.maximemichel.myhome.data.mock.internal

import android.content.res.AssetManager
import java.util.Scanner
import java.util.concurrent.TimeUnit
import okhttp3.mockwebserver.MockResponse

class AssetProvider(
    private val assetManager: AssetManager
) {

    fun createResponseFromAssets(
        fileName: String,
        httpCode: Int = DEFAULT_HTTP_CODE,
        delayInMs: Long = DEFAULT_DELAY_IN_MS
    ): MockResponse {
        val inputStream = assetManager.open(fileName)

        val s = Scanner(inputStream, Charsets.UTF_8.name()).useDelimiter("\\A")
        val result = if (s.hasNext()) s.next() else ""

        return MockResponse()
            .setBody(result)
            .setResponseCode(httpCode)
            .setBodyDelay(delayInMs, TimeUnit.MILLISECONDS)
    }

    fun createResponse(
        content: String,
        httpCode: Int = DEFAULT_HTTP_CODE,
        delayInMs: Long = DEFAULT_DELAY_IN_MS
    ): MockResponse = MockResponse()
        .setBody(content)
        .setResponseCode(httpCode)
        .setBodyDelay(delayInMs, TimeUnit.MILLISECONDS)

    fun createEmptyResponse(
        httpCode: Int = DEFAULT_HTTP_CODE,
        delayInMs: Long = DEFAULT_DELAY_IN_MS
    ): MockResponse = MockResponse()
        .setBody("")
        .setResponseCode(httpCode)
        .setBodyDelay(delayInMs, TimeUnit.MILLISECONDS)

    companion object {
        private const val DEFAULT_DELAY_IN_MS = 1_000L
        private const val DEFAULT_HTTP_CODE = 200
    }
}
