package com.maximemichel.myhome.domain.model

enum class Flavor(private val code: String) {
    MOCK("mock"),
    PROD("prod");

    companion object {
        fun getFlavorFromCode(code: String) = values().first { it.code == code }
    }
}
