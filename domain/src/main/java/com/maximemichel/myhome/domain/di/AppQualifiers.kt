package com.maximemichel.myhome.domain.di

import javax.inject.Qualifier
import kotlin.annotation.AnnotationTarget.FIELD
import kotlin.annotation.AnnotationTarget.FUNCTION
import kotlin.annotation.AnnotationTarget.PROPERTY_GETTER
import kotlin.annotation.AnnotationTarget.PROPERTY_SETTER
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

@Retention(AnnotationRetention.BINARY)
@Qualifier
@Target(FIELD, VALUE_PARAMETER, PROPERTY_GETTER, PROPERTY_SETTER, FUNCTION)
annotation class AppVersionNameMinor

@Retention(AnnotationRetention.BINARY)
@Qualifier
@Target(FIELD, VALUE_PARAMETER, PROPERTY_GETTER, PROPERTY_SETTER, FUNCTION)
annotation class AppVersionName

@Retention(AnnotationRetention.BINARY)
@Qualifier
@Target(FIELD, VALUE_PARAMETER, PROPERTY_GETTER, PROPERTY_SETTER, FUNCTION)
annotation class AppFlavor
