package com.maximemichel.myhome.domain.business.base

import com.maximemichel.myhome.domain.error.mapToError
import com.maximemichel.myhome.domain.model.Result
import com.maximemichel.myhome.domain.model.Result.Error
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn

/**
 * Executes business logic in its execute method and keeps posting updates to the result as [Result<R>][Result].
 *
 * Handling an exception (emit [Result.Error] to the result) is the subclass' responsibility.
 */
abstract class FlowUseCase<in P, R>(private val coroutineDispatcher: CoroutineDispatcher) {

    operator fun invoke(parameters: P): Flow<Result<R>> {
        return execute(parameters)
            .catch { e -> emit(Error(Exception(e).mapToError())) }
            .flowOn(coroutineDispatcher)
    }

    abstract fun execute(parameters: P): Flow<Result<R>>
}
