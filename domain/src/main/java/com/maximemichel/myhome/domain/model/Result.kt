package com.maximemichel.myhome.domain.model

import com.maximemichel.myhome.domain.error.AppError

/**
 * Generic class that holds a value with its loading status.
 *
 * @param R Any type corresponding to the result needed to be held.
 * @constructor Create empty Result
 */
sealed class Result <out R> {

    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val error: AppError) : Result<Nothing>()
    object Loading : Result<Nothing>()

    fun isLoading() = this is Loading

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[data=$error]"
            Loading -> "Loading"
        }
    }
}

/**
 * [Result.data][com.maximemichel.myhome.domain.model.Result.Success.data] if [Result] is of query
 * [Success][com.maximemichel.myhome.domain.model.Result.Success]
 */
fun <T> Result<T>.successOr(fallback: T): T {
    return (this as? Result.Success<T>)?.data ?: fallback
}

inline fun <T> Result<T>.onSuccess(block: (T) -> Unit): Result<T> {
    if (this is Result.Success<T>) {
        block(data)
    }

    return this
}

inline fun <T> Result<T>.onError(block: (AppError) -> Unit): Result<T> {
    if (this is Result.Error) {
        block(error)
    }

    return this
}

inline fun <T> Result<T>.whenFinished(block: () -> Unit): Result<T> {
    block()
    return this
}

val <T> Result<T>.data: T?
    get() = (this as? Result.Success)?.data
