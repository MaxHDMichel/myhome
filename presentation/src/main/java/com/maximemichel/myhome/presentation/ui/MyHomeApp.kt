package com.maximemichel.myhome.presentation.ui

import android.content.res.Configuration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.insets.navigationBarsPadding
import com.maximemichel.myhome.presentation.ui.navigation.Destinations
import com.maximemichel.myhome.presentation.ui.navigation.MyHomeNavGraph
import com.maximemichel.myhome.presentation.ui.theme.MyHomeTheme

@Composable
fun MyHomeApp(currentRoute: String) {
    MyHomeTheme {
        ProvideWindowInsets {
            val navController = rememberNavController()
            val scaffoldState = rememberScaffoldState()
            val darkTheme = isSystemInDarkTheme()

            val items = listOf(
                HomeTab.Dashboard,
                HomeTab.Details,
                HomeTab.Settings
            )

            val navBackStackEntry by navController.currentBackStackEntryAsState()
            val currentDestination = navBackStackEntry?.destination

            val bottomPaddingModifier = if (currentDestination?.route?.contains("welcome") == true) {
                Modifier
            } else {
                Modifier.navigationBarsPadding()
            }

            Scaffold(
                modifier = Modifier
                    .fillMaxSize()
                    .then(bottomPaddingModifier),
                scaffoldState = scaffoldState,
                bottomBar = {
                    if (currentDestination?.route in items.map { it.route }) {
                        BottomNavigation {
                            items.forEach { screen ->
                                BottomNavigationItem(
                                    label = { Text(screen.title) },
                                    icon = {},
                                    selected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
                                    onClick = {
                                        navController.navigate(screen.route) {
                                            // Pop up to the start destination of the graph to
                                            // avoid building up a large stack of destinations
                                            // on the back stack as users select items
                                            popUpTo(navController.graph.findStartDestination().id) {
                                                saveState = true
                                            }
                                            // Avoid multiple copies of the same destination when
                                            // reselecting the same item
                                            launchSingleTop = true
                                            // Restore state when reselecting a previously selected item
                                            restoreState = true
                                        }
                                    }
                                )
                            }
                        }
                    }
                }
            ) { innerPadding ->
                MyHomeNavGraph(
                    modifier = Modifier.padding(innerPadding),
                    navController = navController,
                    startDestination = navBackStackEntry?.destination?.route ?: currentRoute
                )
            }
        }
    }
}

sealed class HomeTab(
    val route: String,
    val title: String
) {
    object Dashboard : HomeTab(
        route = Destinations.Home.HOME_DASHBOARD,
        title = "Dashboard"
    )

    object Details : HomeTab(
        route = Destinations.Home.HOME_DETAILS,
        title = "Details"
    )

    object Settings : HomeTab(
        route = Destinations.Home.HOME_SETTINGS,
        title = "Settings"
    )
}

@Preview(
    name = "MyHomeApp Light theme",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Preview(
    name = "MyHomeApp Dark theme",
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
@Composable
fun MyHomeAppPreview() {
    MyHomeApp(Destinations.Welcome.WELCOME_ROUTE)
}
