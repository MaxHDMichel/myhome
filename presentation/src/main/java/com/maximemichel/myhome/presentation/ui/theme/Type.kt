package com.maximemichel.myhome.presentation.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import com.maximemichel.myhome.presentation.R

val latoFontFamily = FontFamily(
        Font(R.font.lato_black, weight = FontWeight.W900, style = FontStyle.Normal),
        Font(R.font.lato_black_italic, weight = FontWeight.W900, style = FontStyle.Italic),
        Font(R.font.lato_heavy, weight = FontWeight.W800, style = FontStyle.Normal),
        Font(R.font.lato_bold, weight = FontWeight.W700, style = FontStyle.Normal),
        Font(R.font.lato_bold_italic, weight = FontWeight.W700, style = FontStyle.Italic),
        Font(R.font.lato_semibold, weight = FontWeight.W600, style = FontStyle.Normal),
        Font(R.font.lato_medium, weight = FontWeight.W500, style = FontStyle.Normal),
        Font(R.font.lato_regular, weight = FontWeight.W400, style = FontStyle.Normal),
        Font(R.font.lato_italic, weight = FontWeight.W400, style = FontStyle.Italic),
        Font(R.font.lato_light, weight = FontWeight.W300, style = FontStyle.Normal),
        Font(R.font.lato_light_italic, weight = FontWeight.W300, style = FontStyle.Italic),
        Font(R.font.lato_thin, weight = FontWeight.W100, style = FontStyle.Normal),
        Font(R.font.lato_thin_italic, weight = FontWeight.W100, style = FontStyle.Italic)
)

val metropolisFontFamily = FontFamily(
        Font(R.font.metropolis_black, weight = FontWeight.W900, style = FontStyle.Normal),
        Font(R.font.metropolis_black_italic, weight = FontWeight.W900, style = FontStyle.Italic),
        Font(R.font.metropolis_extrabold, weight = FontWeight.W800, style = FontStyle.Normal),
        Font(R.font.metropolis_extrabold_italic, weight = FontWeight.W800, style = FontStyle.Italic),
        Font(R.font.metropolis_bold, weight = FontWeight.W700, style = FontStyle.Normal),
        Font(R.font.metropolis_bold_italic, weight = FontWeight.W700, style = FontStyle.Italic),
        Font(R.font.metropolis_semibold, weight = FontWeight.W600, style = FontStyle.Normal),
        Font(R.font.metropolis_semibold_italic, weight = FontWeight.W600, style = FontStyle.Italic),
        Font(R.font.metropolis_medium, weight = FontWeight.W500, style = FontStyle.Normal),
        Font(R.font.metropolis_medium_italic, weight = FontWeight.W500, style = FontStyle.Italic),
        Font(R.font.metropolis_regular, weight = FontWeight.W400, style = FontStyle.Normal),
        Font(R.font.metropolis_regular_italic, weight = FontWeight.W400, style = FontStyle.Italic),
        Font(R.font.metropolis_light, weight = FontWeight.W300, style = FontStyle.Normal),
        Font(R.font.metropolis_light_italic, weight = FontWeight.W300, style = FontStyle.Italic),
        Font(R.font.metropolis_extralight, weight = FontWeight.W200, style = FontStyle.Normal),
        Font(R.font.metropolis_extralight_italic, weight = FontWeight.W200, style = FontStyle.Italic),
        Font(R.font.metropolis_thin, weight = FontWeight.W100, style = FontStyle.Normal),
        Font(R.font.metropolis_thin_italic, weight = FontWeight.W100, style = FontStyle.Italic)
)
