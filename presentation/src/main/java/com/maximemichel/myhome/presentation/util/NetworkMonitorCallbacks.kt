package com.maximemichel.myhome.presentation.util

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities.TRANSPORT_CELLULAR
import android.net.NetworkCapabilities.TRANSPORT_ETHERNET
import android.net.NetworkCapabilities.TRANSPORT_WIFI
import android.net.NetworkRequest
import android.os.Bundle
import timber.log.Timber

@SuppressLint("MissingPermission")
class NetworkMonitorCallbacks(
    private val connectivityManager: ConnectivityManager
) : Application.ActivityLifecycleCallbacks {

    private var foregroundActivity: Activity? = null
    private val availableNetworkIds = mutableListOf<String>()

    var isNetworkAvailable = connectivityManager.activeNetworkInfo?.isConnected == true

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            availableNetworkIds.add(network.toString())
            propagateNetworkState(true)
            Timber.d("Network available: $network")
        }

        override fun onLost(network: Network) {
            availableNetworkIds.remove(network.toString())
            if (availableNetworkIds.isEmpty()) {
                propagateNetworkState(false)
            }
            Timber.d("Network lost: $network. Available networks: ${availableNetworkIds.size}")
        }

        override fun onUnavailable() {
            propagateNetworkState(false)
            Timber.d("Network unavailable")
        }
    }

    private fun propagateNetworkState(isAvailable: Boolean) {
        foregroundActivity?.let { (it as? NetworkObserver)?.onNetworkAvailableListener(isAvailable) }
    }

    override fun onActivityStarted(activity: Activity) {
        if (activity is NetworkObserver) {
            foregroundActivity = activity
            val networkRequest = NetworkRequest.Builder()
                .addTransportType(TRANSPORT_WIFI)
                .addTransportType(TRANSPORT_CELLULAR)
                .addTransportType(TRANSPORT_ETHERNET)
                .build()

            connectivityManager.registerNetworkCallback(networkRequest, networkCallback)

            val isConnected = connectivityManager.activeNetworkInfo?.isConnected == true
            propagateNetworkState(isConnected)
            Timber.d("Registering network callback.")
        }
    }

    override fun onActivityStopped(activity: Activity) {
        if (activity is NetworkObserver) {
            try {
                connectivityManager.unregisterNetworkCallback(networkCallback)
            } catch (e: IllegalStateException) {
                Timber.d("Network callback was already unregistered.")
            }
            availableNetworkIds.clear()
            foregroundActivity = null
            Timber.d("Unregistering network callback.")
        }
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) = Unit
    override fun onActivityDestroyed(activity: Activity) = Unit
    override fun onActivityPaused(activity: Activity) = Unit
    override fun onActivityResumed(activity: Activity) = Unit
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) = Unit
}

interface NetworkObserver {
    fun onNetworkAvailableListener(isAvailable: Boolean)
}
