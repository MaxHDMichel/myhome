package com.maximemichel.myhome.presentation.ui.welcome

import android.content.res.Configuration
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.maximemichel.myhome.presentation.R
import com.maximemichel.myhome.presentation.ui.components.buttons.MyHomeButton
import com.maximemichel.myhome.presentation.ui.components.buttons.MyHomeTextButton
import com.maximemichel.myhome.presentation.ui.theme.latoFontFamily
import com.maximemichel.myhome.presentation.ui.theme.softGrey

@Composable
fun WelcomeScreen(
    navigateToLogin: () -> Unit,
    navigateToRegister: () -> Unit
) {
    val scaffoldState = rememberScaffoldState()
    val systemUiController = rememberSystemUiController()
    val darkTheme = isSystemInDarkTheme()

    SideEffect {
        systemUiController.setStatusBarColor(Color.Transparent, darkIcons = false)
        systemUiController.setNavigationBarColor(Color.Transparent, darkIcons = true)
    }

    Scaffold(
        scaffoldState = scaffoldState,
    ) { paddingValues ->
        Box(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Image(
                modifier = Modifier
                    .fillMaxSize(),
                painter = painterResource(id = R.drawable.bedroom_image),
                contentDescription = "Welcome background",
                contentScale = ContentScale.Crop
            )
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(530.dp)
                    .background(
                        brush = Brush.verticalGradient(
                            colors = listOf(softGrey, Color.Transparent)
                        )
                    )
                    .align(Alignment.TopCenter),
            )
            Column(
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(horizontal = 16.dp)
                    .padding(top = 120.dp)
                    .fillMaxWidth()

            ) {
                Text(
                    text = "Welcome to your",
                    style = TextStyle(
                        fontFamily = latoFontFamily,
                        fontSize = 36.sp,
                        lineHeight = 48.sp,
                        fontWeight = FontWeight.Normal,
                        color = softGrey
                    )
                )
                Spacer(modifier = Modifier.height(8.dp))
                Text(
                    text = "Home Managment".uppercase(),
                    style = TextStyle(
                        fontFamily = latoFontFamily,
                        fontSize = 36.sp,
                        lineHeight = 56.sp,
                        fontWeight = FontWeight.Light,
                        color = softGrey
                    )
                )
            }
            MyHomeButton(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(horizontal = 16.dp)
                    .padding(bottom = 170.dp)
                    .height(48.dp)
                    .fillMaxWidth(),
                onClick = navigateToLogin,
                text = "Login".uppercase()
            )
            MyHomeTextButton(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(horizontal = 16.dp)
                    .padding(bottom = 70.dp),
                onClick = navigateToRegister,
                text = "Register",
                textDecoration = TextDecoration.Underline
            )
        }
    }
}

@Preview(
    name = "WelcomeScreen Light theme",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Preview(
    name = "WelcomeScreen Dark theme",
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
@Composable
fun WelcomeScreenPreview() {
    WelcomeScreen(
        navigateToLogin = {},
        navigateToRegister = {}
    )
}
