package com.maximemichel.myhome.presentation.ui.components.buttons

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.ButtonElevation
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.maximemichel.myhome.presentation.ui.theme.latoFontFamily

/**
 * Custom text button implementation based on the material [Button]. This button takes a default
 * shape with opposite rounded corners.
 *
 * @param modifier Optional [Modifier] for this composable.
 * @param onClick The action to be executed when the button is clicked.
 * @param text The text to be displayed in the button.
 * @param textStyle A [TextStyle] to apply to the [text]. A default style is provided.
 * @param enabled Whether or not the button is enabled.
 * @param elevation The elevation of the button. The default value is null.
 * @param shape The shape to apply to the button. A default shape is provided.
 * @param border A border to apply to the button. The default value is null.
 * @param colors The [ButtonDefaults.textButtonColors] to apply to the button.
 * @param textDecoration Optional [TextDecoration] to apply to the button text.
 */
@Composable
fun MyHomeTextButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    text: String,
    textStyle: TextStyle = TextStyle(
        fontFamily = latoFontFamily,
        fontSize = 16.sp,
        fontWeight = FontWeight.Normal,
    ),
    enabled: Boolean = true,
    elevation: ButtonElevation? = null,
    shape: Shape = RoundedCornerShape(
        topStart = 16.dp,
        topEnd = 4.dp,
        bottomEnd = 16.dp,
        bottomStart = 4.dp
    ),
    border: BorderStroke? = null,
    colors: ButtonColors = ButtonDefaults.textButtonColors(
        contentColor = Color.White
    ),
    textDecoration: TextDecoration = TextDecoration.None
) {
    MyHomeButton(
        modifier = modifier,
        onClick = onClick,
        text = text,
        textStyle = textStyle,
        enabled = enabled,
        elevation = elevation,
        shape = shape,
        border = border,
        colors = colors,
        textDecoration = textDecoration
    )
}

@Preview(
    name = "MyHomeTextButton Light theme",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Preview(
    name = "MyHomeTextButton Dark theme",
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
@Composable
fun MyHomeTextButtonPreview() {
    MyHomeTextButton(
        text = "Test",
        onClick = {},
        textDecoration = TextDecoration.Underline
    )
}
