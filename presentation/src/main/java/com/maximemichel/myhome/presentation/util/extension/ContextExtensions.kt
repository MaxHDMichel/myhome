package com.maximemichel.myhome.presentation.util.extension

import android.content.Context
import android.net.ConnectivityManager

fun Context.connectivityManager() = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
