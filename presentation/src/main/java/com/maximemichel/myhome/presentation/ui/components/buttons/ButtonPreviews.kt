package com.maximemichel.myhome.presentation.ui.components.buttons

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview

@Preview
@Composable
fun Button() {
    MyHomeButtonPreview()
}

@Preview
@Composable
fun TextButton() {
    MyHomeTextButtonPreview()
}
