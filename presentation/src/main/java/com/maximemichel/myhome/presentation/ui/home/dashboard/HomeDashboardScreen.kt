package com.maximemichel.myhome.presentation.ui.home.dashboard

import android.content.res.Configuration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.maximemichel.myhome.presentation.ui.theme.softWhite

@Composable
fun HomeDashboardScreen(
    navigateToWelcome: () -> Unit
) {
    val scaffoldState = rememberScaffoldState()
    val systemUiController = rememberSystemUiController()
    val darkTheme = isSystemInDarkTheme()

    SideEffect {
        systemUiController.setStatusBarColor(Color.Transparent, darkIcons = !darkTheme)
        systemUiController.setNavigationBarColor(Color.Green, darkIcons = !darkTheme)
    }

    Scaffold(
        modifier = Modifier
            .fillMaxSize(),
        scaffoldState = scaffoldState,
        backgroundColor = if (darkTheme) Color.Red else softWhite,
    ) { paddingValues ->
        Box(
            modifier = Modifier
                .padding(paddingValues)
                .fillMaxSize()
        ) {
            Column(
                modifier = Modifier.align(Alignment.Center)
            ) {
                Text(
                    text = "Home Dashboard Screen",
                    fontSize = 36.sp
                )
                Button(
                    onClick = navigateToWelcome
                ) {
                    Text(
                        text = "To Welcome Screen",
                        fontSize = 36.sp
                    )
                }
            }
        }
    }
}

@Preview(
    name = "HomeDashboardScreen Light theme",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Preview(
    name = "HomeDashboardScreen Dark theme",
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
@Composable
fun HomeDashboardScreenPreview() {
    HomeDashboardScreen(
        navigateToWelcome = {}
    )
}
