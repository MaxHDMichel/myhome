package com.maximemichel.myhome.presentation.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    primary = turquoise,
    primaryVariant = lightBlue,
    secondary = mint,
    background = softGrey
)

private val LightColorPalette = lightColors(
    primary = turquoise,
    primaryVariant = lightBlue,
    secondary = mint,
    background = softWhite
)

@Composable
fun MyHomeTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable() () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        shapes = Shapes,
        content = content
    )
}
