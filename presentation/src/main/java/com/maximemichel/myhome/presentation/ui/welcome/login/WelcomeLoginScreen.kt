package com.maximemichel.myhome.presentation.ui.welcome.login

import android.content.res.Configuration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.navigationBarsPadding
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.maximemichel.myhome.presentation.ui.theme.softWhite

@Composable
fun WelcomeLoginScreen(
    navigateToHome: () -> Unit
) {
    val scaffoldState = rememberScaffoldState()
    val systemUiController = rememberSystemUiController()
    val darkTheme = isSystemInDarkTheme()

    SideEffect {
        systemUiController.setStatusBarColor(Color.Transparent, darkIcons = !darkTheme)
    }

    Scaffold(
        scaffoldState = scaffoldState,
        backgroundColor = if (darkTheme) Color.Magenta else softWhite,
    ) { paddingValues ->
        Box(
            modifier = Modifier
                .padding(paddingValues)
                .fillMaxSize()
                .navigationBarsPadding()
        ) {
            Column(
                modifier = Modifier.align(Alignment.Center)
            ) {
                Text(
                    text = "Login Screen",
                    fontSize = 36.sp
                )
                Button(
                    onClick = navigateToHome
                ) {
                    Text(
                        text = "To Home Screen",
                        fontSize = 36.sp
                    )
                }
            }
        }
    }
}

@Preview(
    name = "WelcomeLoginScreen Light theme",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Preview(
    name = "WelcomeLoginScreen Dark theme",
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
@Composable
fun WelcomeLoginScreenPreview() {
    WelcomeLoginScreen(
        navigateToHome = {}
    )
}
