package com.maximemichel.myhome.presentation.ui.components.buttons

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.ButtonElevation
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.maximemichel.myhome.presentation.ui.theme.cappuccino
import com.maximemichel.myhome.presentation.ui.theme.latoFontFamily
import com.maximemichel.myhome.presentation.ui.theme.softGrey

/**
 * Custom button implementation based on the material [Button]. This button takes a default shape
 * with opposite rounded corners.
 *
 * @param modifier Optional [Modifier] for this composable.
 * @param onClick The action to be executed when the button is clicked.
 * @param text The text to be displayed in the button.
 * @param textStyle A [TextStyle] to apply to the [text]. A default style is provided.
 * @param enabled Whether or not the button is enabled.
 * @param elevation The elevation of the button. The default value is from [ButtonDefaults].
 * @param shape The shape to apply to the button. A default shape is provided.
 * @param border A border to apply to the button. The default value is null.
 * @param colors The [ButtonDefaults.buttonColors] to apply to the button.
 * @param textDecoration Optional [TextDecoration] to apply to the button text.
 * @param contentPadding Content padding to apply to the button. The default value is
 * [ButtonDefaults.ContentPadding].
 */
@Composable
fun MyHomeButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    text: String,
    textStyle: TextStyle = TextStyle(
        fontFamily = latoFontFamily,
        fontSize = 18.sp,
        lineHeight = 20.sp,
        fontWeight = FontWeight.SemiBold,
        letterSpacing = 5.sp
    ),
    enabled: Boolean = true,
    elevation: ButtonElevation? = ButtonDefaults.elevation(),
    shape: Shape = RoundedCornerShape(
        topStart = 16.dp,
        topEnd = 4.dp,
        bottomEnd = 16.dp,
        bottomStart = 4.dp
    ),
    border: BorderStroke? = null,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        backgroundColor = cappuccino,
        contentColor = softGrey
    ),
    textDecoration: TextDecoration = TextDecoration.None,
    contentPadding: PaddingValues = ButtonDefaults.ContentPadding
) {
    Button(
        modifier = modifier,
        onClick = onClick,
        shape = shape,
        enabled = enabled,
        elevation = elevation,
        border = border,
        colors = colors,
        contentPadding = contentPadding
    ) {
        Text(
            text = text,
            style = textStyle,
            textDecoration = textDecoration,
            textAlign = TextAlign.Center
        )
    }
}

@Preview(
    name = "MyHomeButton Light theme",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Preview(
    name = "MyHomeButton Dark theme",
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
@Composable
fun MyHomeButtonPreview() {
    MyHomeButton(
        modifier = Modifier
            .fillMaxWidth()
            .height(48.dp),
        text = "Test",
        onClick = {}
    )
}
