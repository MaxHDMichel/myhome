package com.maximemichel.myhome.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val softWhite = Color(0xFFF9F9F9)
val softGrey = Color(0xFF1A1A1A)
val mint = Color(0xFF60C689)
val turquoise = Color(0xFF57DCBE)
val lightBlue = Color(0xFF57ACDC)
val spBlue = Color(0xFF00A3E0)
val blue = Color(0xFF276BB0)
val darkBlue = Color(0xFF272AB0)
val purple = Color(0xFF5727B0)
val lightPurple = Color(0xFF9C27B0)
val magenta = Color(0xFFC2185B)
val pink = Color(0xFFE91E63)
val cappuccino = Color(0xFFDAB886)
