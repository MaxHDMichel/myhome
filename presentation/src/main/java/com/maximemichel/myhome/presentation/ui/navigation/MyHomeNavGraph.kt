package com.maximemichel.myhome.presentation.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.maximemichel.myhome.presentation.ui.MyHomeApp
import com.maximemichel.myhome.presentation.ui.home.dashboard.HomeDashboardScreen
import com.maximemichel.myhome.presentation.ui.home.details.HomeDetailsScreen
import com.maximemichel.myhome.presentation.ui.home.settings.HomeSettingsScreen
import com.maximemichel.myhome.presentation.ui.welcome.WelcomeScreen
import com.maximemichel.myhome.presentation.ui.welcome.login.WelcomeLoginScreen
import com.maximemichel.myhome.presentation.ui.welcome.register.WelcomeRegisterScreen

/**
 * Destinations use in the [MyHomeApp]
 */
object Destinations {
    object Welcome {
        const val WELCOME_ROUTE = "welcome"
        const val WELCOME_LOGIN_ROUTE = "welcome_login"
        const val WELCOME_REGISTER_ROUTE = "welcome_register"
    }

    object Home {
        const val HOME_DASHBOARD = "home/dashboard"
        const val HOME_DETAILS = "home/details"
        const val HOME_SETTINGS = "home/settings"
    }
}

/**
 * Navigation keys
 */
object NavigationKeys

@Composable
fun MyHomeNavGraph(
    modifier: Modifier = Modifier,
    navController: NavHostController,
    startDestination: String
) {
     val actions = remember(navController) { Actions(navController = navController) }

    NavHost(
        modifier = modifier,
        navController = navController,
        startDestination = startDestination
    ) {
        composable(
            route = Destinations.Welcome.WELCOME_ROUTE
        ) {
            WelcomeScreen(
                navigateToLogin = actions.navigateToWelcomeLogin,
                navigateToRegister = actions.navigateToWelcomeRegister,
            )
        }
        composable(
            route = Destinations.Welcome.WELCOME_LOGIN_ROUTE
        ) {
            WelcomeLoginScreen(
                navigateToHome = actions.navigateToHome
            )
        }
        composable(
            route = Destinations.Welcome.WELCOME_REGISTER_ROUTE
        ) {
            WelcomeRegisterScreen(
                navigateToHome = actions.navigateToHome
            )
        }
        composable(
            route = Destinations.Home.HOME_DASHBOARD
        ) {
            HomeDashboardScreen(
                navigateToWelcome = actions.navigateToWelcome
            )
        }
        composable(
            route = Destinations.Home.HOME_DETAILS
        ) {
            HomeDetailsScreen(
                navigateToWelcome = actions.navigateToWelcome
            )
        }
        composable(
            route = Destinations.Home.HOME_SETTINGS
        ) {
            HomeSettingsScreen(
                navigateToWelcome = actions.navigateToWelcome
            )
        }
    }
}

class Actions(val navController: NavHostController) {
    // Welcome
    val navigateToWelcome = {
        navController.navigate(Destinations.Welcome.WELCOME_ROUTE) {
            popUpTo(Destinations.Welcome.WELCOME_ROUTE) {
                inclusive = true
            }
        }
    }
    val navigateToWelcomeLogin = {
        navController.navigate(Destinations.Welcome.WELCOME_LOGIN_ROUTE)
    }
    val navigateToWelcomeRegister = {
        navController.navigate(Destinations.Welcome.WELCOME_REGISTER_ROUTE)
    }

    // Home
    val navigateToHome = {
        navController.navigate(Destinations.Home.HOME_DASHBOARD) {
            popUpTo(Destinations.Home.HOME_DASHBOARD) {
                inclusive = true
            }
        }
    }

    // Other
    val navigateUp = {
        navController.navigateUp()
    }
}
