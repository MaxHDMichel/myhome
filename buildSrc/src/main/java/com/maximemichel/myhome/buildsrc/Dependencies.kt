package com.maximemichel.myhome.buildsrc

import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit

object Versions {
    private const val MAJOR_VERSION = 1
    private const val MINOR_VERSION = 0
    private const val BUILD_VERSION = 0

    private const val MULTIPLIER_MAJOR = 10_000_000
    private const val MULTIPLIER_MINOR = 100_000
    private const val MULTIPLIER_BUILD = 1_000
    const val TIMEOUT_MINUTES = 60L
    const val VERSION_CODE = 0

    const val COMPILE_SDK = 31

    const val MIN_SDK = 26
    const val TARGET_SDK = 31

    const val NAME: String = "$MAJOR_VERSION.$MINOR_VERSION.$BUILD_VERSION"
    const val MINOR_VERSION_NAME: String = "$MAJOR_VERSION.$MINOR_VERSION"
    val CODE: Int

    init {
        val commitCount = "git rev-list --count HEAD".execute()?.trim()?.toInt() ?: 0
        CODE =
            MAJOR_VERSION * MULTIPLIER_MAJOR + MINOR_VERSION * MULTIPLIER_MINOR + BUILD_VERSION * MULTIPLIER_BUILD + commitCount
    }
}

object Libs {
    const val androidGradlePlugin = "com.android.tools.build:gradle:7.0.4"
    const val dependencyChecker = "com.github.ben-manes:gradle-versions-plugin:0.39.0"

    object Accompanist {
        private const val version = "0.19.0"
        const val insets = "com.google.accompanist:accompanist-insets:$version"
        const val insetsUi = "com.google.accompanist:accompanist-insets-ui:$version"
        const val swiperefresh = "com.google.accompanist:accompanist-swiperefresh:$version"
        const val systemuicontroller = "com.google.accompanist:accompanist-systemuicontroller:$version"
        const val placeholder = "com.google.accompanist:accompanist-placeholder-material:$version"
        const val flowlayout = "com.google.accompanist:accompanist-flowlayout:$version"
    }

    object AndroidX {
        const val appcompat = "androidx.appcompat:appcompat:1.3.0"
        const val recyclerview = "androidx.recyclerview:recyclerview:1.2.0"
        const val swiperefresh = "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"

        object Navigation {
            private const val version = "2.3.5"
            const val fragment = "androidx.navigation:navigation-fragment-ktx:$version"
            const val ui = "androidx.navigation:navigation-ui-ktx:$version"
        }

        object Fragment {
            private const val version = "1.3.4"
            const val fragment = "androidx.fragment:fragment:$version"
            const val fragmentKtx = "androidx.fragment:fragment-ktx:$version"
        }

        const val preference = "androidx.preference:preference:1.1.1"

        const val constraintlayout = "androidx.constraintlayout:constraintlayout:2.1.0"

        const val coreKtx = "androidx.core:core-ktx:1.5.0"

        object Lifecycle {
            private const val version = "2.4.0-rc01"
            const val extensions = "androidx.lifecycle:lifecycle-extensions:$version"
            const val livedata = "androidx.lifecycle:lifecycle-livedata-ktx:$version"
            const val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
            const val runtime = "androidx.lifecycle:lifecycle-runtime-ktx:$version"
        }

        object Room {
            private const val version = "2.4.0-alpha02"
            const val common = "androidx.room:room-common:$version"
            const val runtime = "androidx.room:room-runtime:$version"
            const val compiler = "androidx.room:room-compiler:$version"
            const val ktx = "androidx.room:room-ktx:$version"
            const val testing = "androidx.room:room-testing:$version"
        }

        object Hilt {
            private const val version = "1.0.0"
            const val compiler = "androidx.hilt:hilt-compiler:$version"
        }

        object DataStore {
            private const val version = "1.0.0-beta01"
            const val preferences = "androidx.datastore:datastore-preferences:$version"
        }

        object CameraX {
            private const val version = "1.0.0"
            const val camera = "androidx.camera:camera-camera2:$version"
            const val lifecycle = "androidx.camera:camera-lifecycle:$version"
            const val view = "androidx.camera:camera-view:1.0.0-alpha25"
        }

        const val biometric = "androidx.biometric:biometric:1.1.0"

        object Paging {
            private const val version = "3.0.0"
            const val common = "androidx.paging:paging-common-ktx:$version"
            const val runtime = "androidx.paging:paging-runtime-ktx:$version"
        }

        object Compose {
            const val version = "1.1.0-beta01"
            const val ui = "androidx.compose.ui:ui:$version"
            const val runtime = "androidx.compose.ui:ui:$version"
            const val material = "androidx.compose.material:material:$version"
            const val uiToolingPreview = "androidx.compose.ui:ui-tooling-preview:$version"
            const val uiTooling = "androidx.compose.ui:ui-tooling:$version"
            const val uiTest = "androidx.compose.ui:ui-test-junit4:$version"
            const val activityCompose = "androidx.activity:activity-compose:1.3.0"
            const val hilt = "androidx.hilt:hilt-navigation-compose:1.0.0-alpha03"
            const val icons = "androidx.compose.material:material-icons-extended:$version"
            const val testManifest = "androidx.compose.ui:ui-test-manifest:$version"
            const val constraintLayoutCompose = "androidx.constraintlayout:constraintlayout-compose:1.0.0-beta02"
            const val paging = "androidx.paging:paging-compose:1.0.0-alpha14"
            const val coil = "io.coil-kt:coil-compose:1.4.0"
        }
    }

    object Coroutines {
        private const val version = "1.5.0"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
        const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
        const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
    }

    object Chucker {
        private const val version = "3.5.2"
        const val debug = "com.github.chuckerteam.chucker:library:$version"
        const val release = "com.github.chuckerteam.chucker:library-no-op:$version"
    }

    object Dagger {
        private const val version = "2.38.1"
        const val dagger = "com.google.dagger:dagger:$version"
        const val compiler = "com.google.dagger:dagger-compiler:$version"
    }

    object Detekt {
        private const val version = "1.17.1"
        const val gradlePlugin = "io.gitlab.arturbosch.detekt:detekt-gradle-plugin:$version"
        const val ktlint = "io.gitlab.arturbosch.detekt:detekt-formatting:$version"
    }

    object Google {
        const val material = "com.google.android.material:material:1.3.0"
        const val firebaseCore = "com.google.firebase:firebase-core:19.0.0"
        const val distribution = "com.google.firebase:firebase-appdistribution-gradle:2.2.0"
        const val remoteConfig = "com.google.firebase:firebase-config-ktx:21.0.1"

        const val crashlytics = "com.google.firebase:firebase-crashlytics:18.0.0"
        const val analytics = "com.google.firebase:firebase-analytics-ktx:19.0.0"
        const val crashlyticsGradle = "com.google.firebase:firebase-crashlytics-gradle:2.4.1"

        const val playCore = "com.google.android.play:core:1.10.0"

        const val gmsGoogleServices = "com.google.gms:google-services:4.3.4"

        const val playServicesAuth = "com.google.android.gms:play-services-auth:19.0.0"
        const val playServicesAuthApiPhone =
            "com.google.android.gms:play-services-auth-api-phone:17.5.1"

        const val barcodeScanning = "com.google.mlkit:barcode-scanning:16.1.2"

        const val gson = "com.google.code.gson:gson:2.8.7"
    }

    object Hilt {
        private const val version = "2.38.1"
        const val library = "com.google.dagger:hilt-android:$version"
        const val compiler = "com.google.dagger:hilt-android-compiler:$version"
        const val testing = "com.google.dagger:hilt-android-testing:$version"
        const val gradlePlugin = "com.google.dagger:hilt-android-gradle-plugin:$version"
    }

    object Kotlin {
        const val version = "1.5.31"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"
        const val extensions = "org.jetbrains.kotlin:kotlin-android-extensions:$version"
    }

    object Moshi {
        private const val version = "1.12.0"
        const val moshi = "com.squareup.moshi:moshi:$version"
        const val adapters = "com.squareup.moshi:moshi-adapters:$version"
        const val moshiKotlin = "com.squareup.moshi:moshi-kotlin:$version"
        const val codegen = "com.squareup.moshi:moshi-kotlin-codegen:$version"
    }

    object OkHttp {
        private const val version = "4.9.1"
        const val okhttp = "com.squareup.okhttp3:okhttp:$version"
        const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:$version"
        const val mockWs = "com.squareup.okhttp3:mockwebserver:$version"
    }

    object Retrofit {
        private const val version = "2.9.0"
        const val retrofit = "com.squareup.retrofit2:retrofit:$version"
        const val moshiConverter = "com.squareup.retrofit2:converter-moshi:$version"
        const val scalarConverter = "com.squareup.retrofit2:converter-scalars:$version"
    }

    object Stetho {
        private const val version = "1.6.0"
        const val stetho = "com.facebook.stetho:stetho:$version"
        const val okhttp3 = "com.facebook.stetho:stetho-okhttp3:$version"
    }

    object Zxing {
        const val core = "com.google.zxing:core:3.4.1"
        const val androidCore = "com.google.zxing:android-core:3.3.0"
        const val androidIntegration = "com.google.zxing:android-integration:3.3.0"
    }

    const val cookieJar = "com.github.franmontiel:PersistentCookieJar:v1.0.1"
    const val jsoup = "org.jsoup:jsoup:1.14.1"
    const val lingver = "com.github.YarikSOffice:lingver:1.3.0"
    const val lottie = "com.airbnb.android:lottie:3.7.2"
    const val phoneNumber = "com.googlecode.libphonenumber:libphonenumber:8.12.15"
    const val shimmer = "com.facebook.shimmer:shimmer:0.5.0"
    const val timber = "com.jakewharton.timber:timber:4.7.1"

    // Unit tests
    const val junit5Plugin = "de.mannodermaus.gradle.plugins:android-junit5:1.8.0.0"

    object JUnit {
        private const val version = "5.8.2"
        const val jupiterApi = "org.junit.jupiter:junit-jupiter-api:$version"
        const val jupiterEngine = "org.junit.jupiter:junit-jupiter-engine:$version"
        const val jupiterParams = "org.junit.jupiter:junit-jupiter-params:$version"
        const val vintageEngine = "org.junit.vintage:junit-vintage-engine:$version"
    }

    const val mockitoKotlin = "org.mockito.kotlin:mockito-kotlin:4.0.0"
    const val mockK = "io.mockk:mockk:1.12.1"
    const val jacoco = "org.jacoco:org.jacoco.core:0.8.7" // For code coverage
}

fun String.execute(workingDir: File = File("./")): String? =
    try {
        val parts = this.split("\\s".toRegex())
        val proc = ProcessBuilder(parts)
            .directory(workingDir)
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectError(ProcessBuilder.Redirect.PIPE)
            .start()

        proc.waitFor(Versions.TIMEOUT_MINUTES, TimeUnit.MINUTES)
        proc.inputStream.bufferedReader().readText()
    } catch (e: IOException) {
        e.printStackTrace()
        null
    }
