package task.changelog

class Version(versionName: String) : Comparable<Version> {

    private var major: Int = 0
    private var minor: Int = 0
    private var patch: Int = 0

    init {
        val versionParts = versionName.split(".")

        if (versionParts.size >= 3) {
            major = versionParts[0].toInt()
            minor = versionParts[1].toInt()
            patch = versionParts[2].toInt()
        }
    }

    override fun compareTo(other: Version): Int {
        return when {
            this.major != other.major -> this.major - other.major
            this.minor != other.minor -> this.minor - other.minor
            this.patch != other.patch -> this.patch - other.patch
            else -> 0
        }
    }
}