package task.changelog

import java.io.File
import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes
import java.text.SimpleDateFormat
import java.util.Date
import org.gradle.api.DefaultTask
import org.gradle.api.InvalidUserDataException
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.yaml.snakeyaml.Yaml

/**
 * @author [A745779] Corentin BECT
 */

open class GenerateChangelog : DefaultTask() {

    @InputDirectory
    lateinit var entryDirectory: File

    @OutputFile
    var changelogFile: File = File("${project.buildDir}/tmp/CHANGELOG.md")

    private val dateFormat = SimpleDateFormat("dd-MM-yyyy")

    @TaskAction
    fun generateChangelog() {
        checkPreconditions()

        val changelog = StringBuilder(CHANGELOG_HEADER)

        // Get version directories
        entryDirectory.walk()
            .maxDepth(1)
            // Only get directories and exclude origin directory
            .filter { it.isDirectory && it.name != entryDirectory.name }
            // Sort them by ascending version
            .sortedBy { Version(it.name) }
            // Then parse each version directory and append the content of its entries
            // in the changelog variable
            .forEach { versionDirectory ->
                changelog.append(generateVersionChangelog(versionDirectory))
            }

        // Finally write the parsed changelog in the changelog file
        changelogFile.writeText(changelog.toString())
    }

    private fun generateVersionChangelog(versionDirectory: File): String {
        val versionChangelog = StringBuilder()

        // Get version entries
        val entries = versionDirectory.walk()
            .filter { it.isFile && it.canRead() }

        // Append version header to the changelog if it contains changes
        if (entries.count() > 0) {
            val lastModificationDate = Date(
                entries.maxBy { it.lastModified() }?.lastModified()
                    ?: versionDirectory.lastModified()
            )

            val versionName = versionDirectory.name
            val versionDate: String = dateFormat.format(lastModificationDate)

            val versionHeader = CHANGELOG_VERSION_HEADER_FORMAT.format(versionName, versionDate)
            versionChangelog.append(versionHeader)

            // Group entries by type, sort each group entries by creation date name then parse
            // each change file and append its content in the changelog variable
            entries.map { file ->
                extractChangelogEntryFromFile(file)
            }.groupBy { it.type }
                .map { (type, entries) ->
                    val changelogForType = generateChangelogForType(entries, type)
                    versionChangelog.append(changelogForType)
                }
        }

        return versionChangelog.toString()
    }

    private fun generateChangelogForType(entries: List<ChangelogEntry>, type: String): String {
        val changelogForType = StringBuilder()

        val changeCount = if (entries.size > 1) {
            CHANGE_COUNT_FORMAT
        } else {
            SINGLE_CHANGE_COUNT_FORMAT
        }.format(entries.size)

        val typeHeader = CHANGELOG_ENTRY_TYPE_FORMAT.format(type.capitalize(), changeCount)
        changelogForType.append(typeHeader)
        entries.sortedBy { it.creationTime }
            .forEach { entry ->
                changelogForType.append(generateChangelogForEntry(entry))
            }

        return changelogForType.toString()
    }

    private fun generateChangelogForEntry(entry: ChangelogEntry): String {
        return CHANGELOG_ENTRY_FORMAT.format(entry.description)
    }

    private fun extractChangelogEntryFromFile(file: File): ChangelogEntry {
        val changelogEntry = Yaml().loadAs(file.readText(), ChangelogEntry::class.java)

        val attr = Files.readAttributes(file.toPath(), BasicFileAttributes::class.java)
        changelogEntry.creationTime = attr.creationTime().toMillis()

        return changelogEntry
    }

    private fun checkPreconditions() {
        if (!entryDirectory.exists()) {
            throw InvalidUserDataException("[GenerateChangelog] Input entry directory does not exist.")
        }

        if (!entryDirectory.isDirectory) {
            throw InvalidUserDataException("[GenerateChangelog] Input entry directory is not a directory.")
        }
    }

    companion object {
        private const val CHANGELOG_HEADER = "# Change Log\n"
        private const val CHANGELOG_VERSION_HEADER_FORMAT = "\n## Version %s (%s)\n"
        private const val CHANGELOG_ENTRY_FORMAT = "- %s\n"
        private const val CHANGELOG_ENTRY_TYPE_FORMAT = "\n### %s (%s)\n\n"
        private const val SINGLE_CHANGE_COUNT_FORMAT = "%d change"
        private const val CHANGE_COUNT_FORMAT = "%d changes"
    }
}
