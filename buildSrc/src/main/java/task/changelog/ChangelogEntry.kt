package task.changelog

class ChangelogEntry {
    lateinit var description: String
    lateinit var type: String
    var creationTime: Long = 0
}