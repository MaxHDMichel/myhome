package task

import Const.ENV_CI_CASH
import Const.ENV_PROJECT_ID
import java.util.logging.Level
import org.gitlab4j.api.GitLabApi
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

open class TagVersion : DefaultTask() {

    @Input
    lateinit var endpoint: String

    @Input
    lateinit var token: String

    @Input
    lateinit var envVariant: String

    @Input
    lateinit var currentVersion: String

    private val gitlabProjectId by lazy { System.getenv()[ENV_PROJECT_ID] }
    private val gitlabHash by lazy { System.getenv()[ENV_CI_CASH] }
    private val client by lazy { GitLabApi(endpoint, token).withRequestResponseLogging(Level.ALL) }

    @TaskAction
    fun createTagForVersion() {
        val baseTagName = "${envVariant.toUpperCase()}-$currentVersion"
        val tags = client.tagsApi.getTags(gitlabProjectId)
            .map { it.name }

        var expectedTagName: String = baseTagName
        var tagOccurence = 2

        while (tags.contains(expectedTagName)) {
            expectedTagName = "$baseTagName-$tagOccurence"
            tagOccurence++
        }

        client.tagsApi.createTag(gitlabProjectId, expectedTagName, gitlabHash)
    }
}