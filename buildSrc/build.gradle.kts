repositories {
    mavenCentral()
}
plugins {
    `kotlin-dsl`
}

dependencies {
    implementation("org.gitlab4j:gitlab4j-api:4.14.25")
    implementation("org.yaml:snakeyaml:1.28")
}
